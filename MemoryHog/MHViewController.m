//
//  MHViewController.m
//  MemoryHog
//
//  Created by Cory Powers on 9/18/13.
//  Copyright (c) 2013 Cory Powers. All rights reserved.
//

#import "MHViewController.h"
#include <stdlib.h>

#define CHUNK_SIZE 1000000

@interface MHViewController ()
@property (strong, nonatomic) IBOutlet UILabel *bytesLabel;
@property (assign, nonatomic) long long bytesAllocated;
@property (strong, atomic) NSMutableArray *blockPointers;
@property (assign, atomic) BOOL running;
@property (strong, nonatomic) IBOutlet UIButton *stopButton;
- (IBAction)clearAction:(id)sender;

- (IBAction)stopAction:(id)sender;
- (void)allocBytes;
@end

@implementation MHViewController

- (void)viewDidLoad{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	self.blockPointers = [NSMutableArray arrayWithCapacity:5];
	self.running = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		[self allocBytes];
	});
}

- (IBAction)clearAction:(id)sender {
	self.blockPointers = [NSMutableArray arrayWithCapacity:5];
	self.bytesAllocated = 0;
	self.bytesLabel.text = @"0 MB Used";
}

- (IBAction)stopAction:(id)sender {
	if (self.running == YES) {
		self.running = NO;
		[self.stopButton setTitle:@"Start" forState:UIControlStateNormal];
	}else{
		self.running = YES;
		[self.stopButton setTitle:@"Stop" forState:UIControlStateNormal];
		
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
			[self allocBytes];
		});
	}
}

- (void)allocBytes{
	if (self.running == NO) {
		return;
	}
	NSMutableData *chunk = [NSMutableData dataWithCapacity:CHUNK_SIZE];
	for (int i = 0; i < CHUNK_SIZE; i += sizeof(int32_t)) {
		int32_t randomByte = arc4random_uniform(INT32_MAX);
		[chunk appendBytes:&randomByte length:sizeof(int32_t)];
	}
	self.bytesAllocated += CHUNK_SIZE;
	[self.blockPointers addObject:chunk];
	dispatch_async(dispatch_get_main_queue(), ^{
		self.bytesLabel.text = [NSString stringWithFormat:@"%lld MB Used", self.bytesAllocated/1000000];
	});
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		[self allocBytes];
	});
}
@end
