//
//  MHAppDelegate.h
//  MemoryHog
//
//  Created by Cory Powers on 9/18/13.
//  Copyright (c) 2013 Cory Powers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MHAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
