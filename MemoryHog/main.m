//
//  main.m
//  MemoryHog
//
//  Created by Cory Powers on 9/18/13.
//  Copyright (c) 2013 Cory Powers. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MHAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([MHAppDelegate class]));
	}
}
